AdvancedReport
The plugin that will help your staff team!

**What the plugin does?**
The plugin allowes you to punish and report players in an easy way! Just type the command and choose the report/punishment from the GUI!

**Why should I use this plugin and not other one?**
You should use this plugin because this plugin is GUIs built. It is easy to report and punish players without typing alot of things in the chat.
**
How can I report a player?**
To report a player, you have to type the command "/report" in the chat.
After that, it will open you a GUI.

**What the report will do?**
The report will send a message to all the staff members that have the permission: "advancedreport.staff"!
The message will look like that:

**How can a staff member punish a player?**
The staff member have to type the command "/punish" and a GUI will open to him. He will have to choose a punishment for the player. (NOTE: You can edit the ban time through the config file! Just follow the instructions inside the config file!)

**
How do I disable the punishment to a player?**
To un-punish a player. A player with the permission "advancedreport.unpunish" have to run the command "/unpunish".
It will open a GUI for him with options to unban a player and unmute a player!


Commands List:

    Report: Report a player about any types of hacks/spam!
    Punish: Punish a player with a punishment from the list!
    Unpunish: Un-punish any player!
    Arc: Reloades the config!

Punishments:

    Ban for PvP Hacks
    Ban for Movment Hacks (Including fly and speed)
    Mute a player
    Unlimited ban

Permissins:

    advancedreport.staff: Allowes players to see reports!
    advancedreport.report: Allowes players to report anyone!
    advancedreport.punish: Allowes to punish players!
    advancedreport.unpunish: Allowes to un-punish players!
    advancedreport.reload: Allowes player to reload the config!

If you have any problems with the plugin, contact me on in my mail: amitzw9@gmail.com :)