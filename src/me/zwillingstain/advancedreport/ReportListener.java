package me.zwillingstain.advancedreport;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class ReportListener implements Listener {
	
	@EventHandler
	public void onInv(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().getName().equals(ReportGUI.rep())) {
			if (e.getCurrentItem().getType() == Material.DIAMOND_SWORD) {
				p.sendMessage("�b[AdvancedReport] �aYou have been reported �e" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + " �afor PvP Hacking");
				for (Player s:Bukkit.getOnlinePlayers()) {
					if (s.hasPermission("advancedreport.staff")) {
						s.sendMessage("�b[AdvancedReport] �a" + p.getName() + " �eHave been reported �c" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + " �efor PvP Hacking");
					}
				}
			}
			if (e.getCurrentItem().getType() == Material.FEATHER) {
				p.sendMessage("�b[AdvancedReport] �aYou have been reported �e" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + " �afor Fly Hacking");
				for (Player s:Bukkit.getOnlinePlayers()) {
					if (s.hasPermission("advancedreport.staff")) {
						s.sendMessage("�b[AdvancedReport] �a" + p.getName() + " �eHave been reported �c" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + " �efor Fly Hacking");
					}
				}
			}
			if (e.getCurrentItem().getType() == Material.CHAINMAIL_BOOTS) {
				p.sendMessage("�b[AdvancedReport] �aYou have been reported �e" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + " �afor Movment Hacks");
				for (Player s:Bukkit.getOnlinePlayers()) {
					if (s.hasPermission("advancedreport.staff")) {
						s.sendMessage("�b[AdvancedReport] �a" + p.getName() + " �eHave been reported �c" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + " �efor Movment Hacks");
					}
				}
			}
			if (e.getCurrentItem().getType() == Material.BOOK_AND_QUILL) {
				p.sendMessage("�b[AdvancedReport] �aYou have been reported �e" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + "�afor Spam in chat");
				for (Player s:Bukkit.getOnlinePlayers()) {
					if (s.hasPermission("advancedreport.staff")) {
						s.sendMessage("�b[AdvancedReport] �a" + p.getName() + " �eHave been reported �c" + Bukkit.getPlayer(Main.target.get(p.getUniqueId())).getName() + " �efor Spam in chat");
					}
				}
			}
		}
	}

}
