package me.zwillingstain.advancedreport;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	public static JavaPlugin plugin;
	public static Map<UUID, UUID> target = new HashMap<UUID, UUID>();
	public static Map<UUID, UUID> punish = new HashMap<UUID, UUID>();
	public static Map<UUID, UUID> unpunish = new HashMap<UUID, UUID>();
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new PunishListener(), this);
		getServer().getPluginManager().registerEvents(new ReportListener(), this);
		plugin = this;
		getConfig().options().copyDefaults(true);
		saveConfig();
		reloadConfig();
		}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {

		Player p = (Player) sender;
		if (cmd.equalsIgnoreCase("report")) {
			
			if (p.hasPermission("advancedreport.report")) {
				if (args.length == 1) {
					if (Bukkit.getPlayerExact(args[0]) != null) {
						p.openInventory(ReportGUI.rep());
						target.put(p.getUniqueId(), Bukkit.getPlayerExact(args[0]).getUniqueId());
					} else
						p.sendMessage("�b[AdvancedReport] �cPlayer is not online!");
				} else
					p.sendMessage("�b[AdvancedReport] �cYou have to enter one name!");
			} else
				p.sendMessage("�b[AdvancedReport] �cYou don't have permissions to use this command!");
		}
		
		if (cmd.equalsIgnoreCase("punish")) {
			if (p.hasPermission("advancedreport.punish")) {
				if (args.length == 1) {
					if (Bukkit.getPlayerExact(args[0]) != null) {
						p.openInventory(ReportGUI.pun());
						punish.put(p.getUniqueId(), Bukkit.getPlayerExact(args[0]).getUniqueId());
					} else
						p.sendMessage("�b[AdvancedReport] �cYou can't punish offline players!");
				} else
					p.sendMessage("�b[AdvancedReport] �cYou have to enter one name!");
			} else
				p.sendMessage("�b[AdvancedReport] �cYou don't have permissions to punish players!");
		}
		
		if (cmd.equalsIgnoreCase("unpunish")) {
			if (p.hasPermission("advancedreport.unpunish")) {
				if (args.length == 1) {
					p.openInventory(ReportGUI.dis());
					unpunish.put(p.getUniqueId(), Bukkit.getPlayerExact(args[0]).getUniqueId());
				} else
					p.sendMessage("�b[AdvancedReport] �cYou have to enter one name!");
			} else
				p.sendMessage("�b[AdvancedReport] �cYou don't have permissions to un-punish players!");
		}
		
		if (cmd.equalsIgnoreCase("arc")) {
			if (p.hasPermission("advancedreport.reload")) {
				reloadConfig();
				p.sendMessage("�b[AdvancedReport] �aConfig Reloaded!");
			}
		}
		
		return false;
	}

}
