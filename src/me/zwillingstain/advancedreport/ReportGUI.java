package me.zwillingstain.advancedreport;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

public class ReportGUI {

	public static Inventory rep() {

		Inventory rep = Bukkit.createInventory(null, 1 * 9);
		AdvancedItem pvp = new AdvancedItem(Material.DIAMOND_SWORD, 1, (short) 0, "�ePvP Hacks",
				"�5Report players about pvp hacking");
		pvp.setGlow(true);
		rep.setItem(1, pvp);

		AdvancedItem fly = new AdvancedItem(Material.FEATHER, 1, (short) 0, "�eFly",
				"�5Report players about fly hacking");
		rep.setItem(3, fly);

		AdvancedItem mov = new AdvancedItem(Material.CHAINMAIL_BOOTS, 1, (short) 0, "�eMovment Hacks",
				"�5Report players about movment hacks");
		rep.setItem(5, mov);

		AdvancedItem spam = new AdvancedItem(Material.BOOK_AND_QUILL, 1, (short) 0, "�eSpam",
				"�5Report players about spamming in chat");
		rep.setItem(7, spam);

		return rep;

	}

	public static Inventory pun() {

		Inventory pun = Bukkit.createInventory(null, 1 * 9);
		AdvancedItem cos = new AdvancedItem(Material.PAPER, -1, (short) 0, "�eUnlimited Ban", "�5Ban a player forever");
		cos.setGlow(true);
		pun.setItem(1, cos);

		AdvancedItem mut = new AdvancedItem(Material.BOOK_AND_QUILL, 1, (short) 0, "�eMute", "�5Mute player");
		pun.setItem(3, mut);

		AdvancedItem pvp = new AdvancedItem(Material.DIAMOND_SWORD, 1, (short) 0, "�ePvP Hacking",
				"�5Ban a player for PvP Hacks");
		pun.setItem(5, pvp);

		AdvancedItem mov = new AdvancedItem(Material.CHAINMAIL_BOOTS, 1, (short) 0, "�eMovment Hacks",
				"�5Ban a player for movment hacks");
		pun.setItem(7, mov);

		return pun;
	}

	public static Inventory mut() {

		Inventory mut = Bukkit.createInventory(null, 1 * 9);

		AdvancedItem fmin = new AdvancedItem(Material.PAPER, 5, (short) 0, "�e5 Minutes",
				"�5Mute a player for 5 minutes");
		mut.setItem(1, fmin);

		AdvancedItem hhur = new AdvancedItem(Material.PAPER, 30, (short) 0, "�e30 Minutes",
				"�5Mute a player for 30 minutes");
		mut.setItem(3, hhur);

		AdvancedItem hour = new AdvancedItem(Material.PAPER, 60, (short) 0, "�eOne Hour",
				"�5Mute a player for one hour");
		mut.setItem(5, hour);

		AdvancedItem unl = new AdvancedItem(Material.PAPER, -1, (short) 0, "�eUnlimited Mute",
				"�5Mute a player forever");
		unl.setGlow(true);
		mut.setItem(7, unl);

		return mut;
	}

	public static Inventory mov() {

		Inventory mov = Bukkit.createInventory(null, 1 * 9);

		AdvancedItem fly = new AdvancedItem(Material.CHAINMAIL_BOOTS, 1, (short) 0, "�eFly Hacking",
				"�5Ban a player for fly hacking");
		mov.setItem(1, fly);

		AdvancedItem speed = new AdvancedItem(Material.FEATHER, 1, (short) 0, "�eSpeed Hacking",
				"�5Ban a player for speed hacking");
		mov.setItem(3, speed);

		AdvancedItem schd = new AdvancedItem(Material.WOOL, 1, (short) 3, "�eScaffholding",
				"�5Ban a player for Scaffholding");
		mov.setItem(5, schd);

		AdvancedItem nosl = new AdvancedItem(Material.BOW, 1, (short) 0, "�eNo Slowdown",
				"�5Ban a player for no slowdown hacks");
		mov.setItem(7, nosl);

		return mov;
	}
	
	public static Inventory dis() {
		
		Inventory dis = Bukkit.createInventory(null, 1*9);
		
		AdvancedItem unb = new AdvancedItem(Material.PAPER, -1, (short)0, "�aUnban a player!", "�5Unban a player from the server!");
		unb.setGlow(true);
		dis.setItem(4, unb);
		
		AdvancedItem unm = new AdvancedItem(Material.BOOK_AND_QUILL, -1, (short)0, "�aUn-mute a player!", "�5Un-mute any player!");
		unm.setGlow(true);
		dis.setItem(6, unm);
		
		return dis;
	}

}
