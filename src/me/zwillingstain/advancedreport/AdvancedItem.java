package me.zwillingstain.advancedreport;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * You can use AdvancedItem instead of the regular ItemStack. AdvancedItem is easier and contains more options. 
 * 
 * @author DrBenana
 */
public class AdvancedItem extends ItemStack {
	
	/**
	 * @param item An ItemStack to clone.
	 */
	public AdvancedItem(ItemStack item) {
		super(item);
		
	}
	
	/**
	 * @param material Material to create an ItemStack from.
	 */
	public AdvancedItem(Material material) {
		super(material);
	}
	
	/**
	 * @param material Material to create an ItemStack from.
	 * @param amount The amount of the items.
	 */
	public AdvancedItem(Material material, int amount) {
		super(material, amount);
	}
	
	/**
	 * @param material Material to create an ItemStack from.
	 * @param amount The amount of the items.
	 * @param data Damage, Durability (Max-Durability), Data.
	 */
	public AdvancedItem(Material material, int amount, short data) {
		super(material, amount, data);
	}
	
	/**
	 * 
	 * @param material Material to create an ItemStack from.
	 * @param amount The amount of the items.
	 * @param data Damage, Durability (Max-Durability), Data.
	 * @param name The name of the item.
	 */
	public AdvancedItem(Material material, int amount, short data, String name) {
		super(material, amount, data);
		this.setDisplayName(name);
	}
	
	/**
	 * 
	 * @param material Material to create an ItemStack from.
	 * @param amount The amount of the items.
	 * @param data Damage, Durability (Max-Durability), Data.
	 * @param name The name of the item.
	 * @param lore The lore of the item (Multi-Line).
	 */
	public AdvancedItem(Material material, int amount, short data, String name, String... lore) {
		super(material, amount, data);
		this.setDisplayName(name);
		this.setLore(lore);
	}
	
	/**
	 * @param name A new name for the item.
	 * @return An ItemStackManager of the item.
	 */
	public AdvancedItem setDisplayName(String name) {
		ItemMeta meta = getItemMeta();
		meta.setDisplayName(name);
		setItemMeta(meta);
		return this;
	}
	
	/**
	 * @param lore A new lore for the item. It can be more than one line.
	 * @return An ItemStackManager of the item.
	 */
	public AdvancedItem setLore(String... lore) {
		ItemMeta meta = getItemMeta();
		meta.setLore(Arrays.asList(lore));
		setItemMeta(meta);
		return this;
	}
	
	/**
	 * @param glow true to add glow, false to remove.
	 * @return An ItemStackManager of the item.
	 */
	public AdvancedItem setGlow(boolean glow) {
		ItemMeta meta = getItemMeta();
		if (glow) meta.addEnchant(new Glow(), 1, true);
		else meta.removeEnchant(new Glow());
		setItemMeta(meta);
		return this;
	}
	
	/**
	 * A Glow class. It's like an enchant without a name.
	 * 
	 * @author DrBenana
	 */
	public static class Glow extends Enchantment {

		public Glow() {
			super(132);
		}

		@Override
		public boolean canEnchantItem(ItemStack arg0) {
			return false;
		}

		@Override
		public boolean conflictsWith(Enchantment arg0) {
			return false;
		}

		@Override
		public EnchantmentTarget getItemTarget() {
			return null;
		}

		@Override
		public int getMaxLevel() {
			return 0;
		}

		@Override
		public String getName() {
			return null;
		}

		@Override
		public int getStartLevel() {
			return 0;
		}
		
	}
}