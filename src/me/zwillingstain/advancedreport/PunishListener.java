package me.zwillingstain.advancedreport;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PunishListener implements Listener {
	public static List<UUID> muted = new ArrayList<>();
	
	@EventHandler
	@SuppressWarnings("deprecation")
	public void onInv(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().getName().equals(ReportGUI.pun().getName())) {
			e.setCancelled(true);
			if (e.getCurrentItem().getType() == Material.PAPER) {
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(true);
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).kickPlayer("�b[AdvancedReport] �cYou got unlimited ban from the server by a moderator!");
				p.sendMessage("�b[AdvancedReport] �e" + Bukkit.getPlayer(Main.punish.get(p.getUniqueId())) + " banned from the server!");
			}
			if (e.getCurrentItem().getType() == Material.BOOK_AND_QUILL) {
				p.openInventory(ReportGUI.mut());
			}
			if (e.getCurrentItem().getType() == Material.DIAMOND_SWORD) {
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(true);
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).kickPlayer("�b[AdvancedReport] �cYou got banned by a moderator. Reason: PvP Hacks");
				p.sendMessage("�b[AdvancedReport] �e" + Bukkit.getPlayer(Main.punish.get(p.getUniqueId())) + " banned for PvP Hacks!");
				new BukkitRunnable() {
					@Override
					public void run() {
						Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(false);
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*60*24*Main.plugin.getConfig().getInt("BanTime.PvPHacks"));
			}
			if (e.getCurrentItem().getType() ==  Material.CHAINMAIL_BOOTS) {
				p.openInventory(ReportGUI.mov());
			}
		}
		if (e.getInventory().getName().equals(ReportGUI.mut().getName())) {
			e.setCancelled(true);
			if (e.getCurrentItem().getAmount() == 5) {
				muted.add(Main.punish.get(p.getUniqueId()));
				new BukkitRunnable() {
					@Override
					public void run() {
						muted.remove(Main.punish.get(p.getUniqueId()));
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*5);
			}
			if (e.getCurrentItem().getAmount() == 30) {
				muted.add(Main.punish.get(p.getUniqueId()));
				new BukkitRunnable() {
					@Override
					public void run() {
						muted.remove(Main.punish.get(p.getUniqueId()));
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*30);
			}
			if (e.getCurrentItem().getAmount() == 60) {
				muted.add(Main.punish.get(p.getUniqueId()));
				new BukkitRunnable() {
					@Override
					public void run() {
						muted.remove(Main.punish.get(p.getUniqueId()));
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*60);
			}
			if (e.getCurrentItem().getAmount() == -1) {
				muted.add(Main.punish.get(p.getUniqueId()));
			}
		}
		if (e.getInventory().getName().equals(ReportGUI.mov().getName())) {
			e.setCancelled(true);
			if (e.getCurrentItem().getType() == Material.CHAINMAIL_BOOTS) {
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(true);
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).kickPlayer("�b[AdvancedReport] �cYou got banned by a moderator. Reason: Fly hacking");
				p.sendMessage("�b[AdvancedReport] �e" + Bukkit.getPlayer(Main.punish.get(p.getUniqueId())) + " banned for Fly Hacking!");
				new BukkitRunnable() {
					@Override
					public void run() {
						Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(false);
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*60*24*Main.plugin.getConfig().getInt("BanTime.FlyHacks"));
			}
			if (e.getCurrentItem().getType() == Material.FEATHER) {
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(true);
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).kickPlayer("�b[AdvancedReport] �cYou got banned by a moderator. Reason: Speed hacking");
				p.sendMessage("�b[AdvancedReport] �e" + Bukkit.getPlayer(Main.punish.get(p.getUniqueId())) + " banned for Speed Hacking!");
				new BukkitRunnable() {
					@Override
					public void run() {
						Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(false);
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*60*24*Main.plugin.getConfig().getInt("BanTime.Speed"));
			}
			if (e.getCurrentItem().getType() == Material.WOOL) {
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(true);
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).kickPlayer("�b[AdvancedReport] �cYou got banned by a moderator. Reason: Scaffholding");
				p.sendMessage("�b[AdvancedReport] �e" + Bukkit.getPlayer(Main.punish.get(p.getUniqueId())) + " banned for Scaffholding!");
				new BukkitRunnable() {
					@Override
					public void run() {
						Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(false);
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*60*24*Main.plugin.getConfig().getInt("BanTime.Scaffholding"));
			}
			if (e.getCurrentItem().getType() == Material.BOW) {
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(true);
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).kickPlayer("�b[AdvancedReport] �cYou got banned by a moderator. Reason: No Slowdown");
				p.sendMessage("�b[AdvancedReport] �e" + Bukkit.getPlayer(Main.punish.get(p.getUniqueId())) + " banned for No Slowdown!");
				new BukkitRunnable() {
					@Override
					public void run() {
						Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(false);
					}
				}.runTaskLaterAsynchronously(Main.plugin, 20*60*60*24*Main.plugin.getConfig().getInt("BanTime.NoSlowdown"));
			}
		}
		if (e.getInventory().getName().equals(ReportGUI.dis().getName())) {
			e.setCancelled(true);
			if (e.getCurrentItem().getType() == Material.PAPER) {
				Bukkit.getPlayer(Main.punish.get(p.getUniqueId())).setBanned(false);
				p.sendMessage("�b[AdvancedReport] �aUnbanned successfully!");
			}
			if (e.getCurrentItem().getType() == Material.BOOK_AND_QUILL) {
				muted.remove(Main.punish.get(p.getUniqueId()));
				p.sendMessage("�b[AdvancedReport] �aUn-muted successfully!");
			}
		}
	}
	
	@EventHandler
	public void onChatEvent(AsyncPlayerChatEvent e) {
		
		if (muted.contains(e.getPlayer().getUniqueId())) {
			e.setCancelled(true);
		}
	}

}
